<?php

namespace DomotronCloudClient\Model\Item;

class User extends Item
{
    /**
     * Process data
     */
    protected function processData()
    {
        if (isset($this->data['userAddresses'])) {
            $userAddressesData = $this->data['userAddresses']['data'];
            $this->data['userAddresses'] = [];
            foreach ($userAddressesData as $userAddress) {
                $this->data['userAddresses'][] = new UserAddress($userAddress);
            }
        }

        if (isset($this->data['userPartners'])) {
            $userPartnersData = $this->data['userPartners']['data'];
            $this->data['userPartners'] = [];
            foreach ($userPartnersData as $userPartner) {
                $this->data['userPartners'][] = new UserPartner($userPartner);
            }
        }
    }
}
