<?php

namespace DomotronCloudClient\Model\Item;

class Project extends Item
{
    /**
     * Process data
     */
    protected function processData()
    {
        if (isset($this->data['object'])) {
            $this->data['object'] = new Obj($this->data['object']);
        }

        if (isset($this->data['salesman'])) {
            $this->data['salesman'] = new Salesman($this->data['salesman']);
        }
    }
}
