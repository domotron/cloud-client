<?php

namespace DomotronCloudClient\Exception;

class BadResponseException extends DriverException
{
    /** @var mixed */
    private $data;

    /**
     * @param mixed $data
     * @return BadResponseException
     */
    public function setDebugData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDebugData()
    {
        return $this->data;
    }
}
