<?php

namespace DomotronCloudClient\Endpoint;

use DomotronCloudClient\Model\Collection\ProjectCollection;
use DomotronCloudClient\Model\Item\Project;

class ProjectEndpoint extends Endpoint
{
    /**
     * Fetch project by id
     * @param int $id
     * @param string|null $userToken
     * @return Project
     */
    public function get($id, $userToken = null)
    {
        return $this->wrapWithQueryProcess('getProject', function () use ($id, $userToken) {
            return $this->driver->getProject($id, $userToken);
        });
    }

    /**
     * Fetch project collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return ProjectCollection
     */
    public function listing($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        return $this->wrapWithQueryProcess('listingProjects', function () use ($page, $limit, $search, $searchIn, $userToken) {
            return $this->driver->listingProjects($page, $limit, $search, $searchIn, $userToken);
        });
    }

    /**
     * Create new project
     * @param array $data
     * @param string|null $userToken
     * @return Project
     */
    public function create(array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('createProject', function () use ($data, $userToken) {
            return $this->driver->createProject($data, $userToken);
        });
    }

    /**
     * Update existing project
     * @param $id
     * @param array $data
     * @param string|null $userToken
     * @return Project
     */
    public function update($id, array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('updateProject', function () use ($id, $data, $userToken) {
            return $this->driver->updateProject($id, $data, $userToken);
        });
    }
}