<?php

namespace DomotronCloudClient\Endpoint;

use DomotronCloudClient\Model\Collection\ObjectCollection;
use DomotronCloudClient\Model\Item\Obj;

class ObjectEndpoint extends Endpoint
{
    /**
     * Fetch object by id
     * @param int $id
     * @param string|null $userToken
     * @return Obj
     */
    public function get($id, $userToken = null)
    {
        return $this->wrapWithQueryProcess('getObject', function () use ($id, $userToken) {
            return $this->driver->getObject($id, $userToken);
        });
    }

    /**
     * Fetch object collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return ObjectCollection
     */
    public function listing($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        return $this->wrapWithQueryProcess('listingObjects', function () use ($page, $limit, $search, $searchIn, $userToken) {
            return $this->driver->listingObjects($page, $limit, $search, $searchIn, $userToken);
        });
    }

    /**
     * Create new object
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     */
    public function create(array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('createObject', function () use ($data, $userToken) {
            return $this->driver->createObject($data, $userToken);
        });
    }

    /**
     * Update existing object
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     */
    public function update($id, array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('updateObject', function () use ($id, $data, $userToken) {
            return $this->driver->updateObject($id, $data, $userToken);
        });
    }
}
