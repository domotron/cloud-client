<?php

namespace DomotronCloudClient\Endpoint;

use DomotronCloudClient\Model\Collection\MarketCollection;
use DomotronCloudClient\Model\Item\Market;

class MarketEndpoint extends Endpoint
{
    /**
     * Fetch market by id
     * @param int $id
     * @param string|null $userToken
     * @return Market
     */
    public function get($id, $userToken = null)
    {
        return $this->wrapWithQueryProcess('getMarket', function () use ($id, $userToken) {
            return $this->driver->getMarket($id, $userToken);
        });
    }

    /**
     * Fetch market collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return MarketCollection
     */
    public function listing($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        return $this->wrapWithQueryProcess('listingMarkets', function () use ($page, $limit, $search, $searchIn, $userToken) {
            return $this->driver->listingMarkets($page, $limit, $search, $searchIn, $userToken);
        });
    }

    /**
     * Create new market
     * @param array $data
     * @param string|null $userToken
     * @return Market
     */
    public function create(array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('createMarket', function () use ($data, $userToken) {
            return $this->driver->createMarket($data, $userToken);
        });
    }

    /**
     * Update existing market
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Market
     */
    public function update($id, array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('updateMarket', function () use ($id, $data, $userToken) {
            return $this->driver->updateMarket($id, $data, $userToken);
        });
    }
}
